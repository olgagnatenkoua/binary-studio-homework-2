export default {
  FAILED_TO_LOAD_TEXT: "Failed to load data",
  START_NEW_GAME: "Start New Game",
  FIGHT: "Fight",
  DRAW: "Draw",
  NEXT_ROUND: "Next Round",
  UPDATE: "Update"
};
